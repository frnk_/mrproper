#!/usr/bin/env bash

# Proyect date: 2017.12.16
# Limpia los nombres de archivos de video de cadenas tipo 'XviD/[DivxTotal]/etc..'
# El archivo 'strings_cleanner_sed_list.sh' almacena todas las cadenas a modificar.

IFS=$'\n'
STRINGS="$HOME/bin/mrproper.regex"
N='\033[00m'      # None
G='\033[01;32m'   # Green
R='\033[01;31m'   # Red
TEST_RENAME='T'   # Test

# Verifica si estamos en el directorio correcto
function _correct_dir() {
	while :; do
	read -r -n1 -p $'Modificar archivos en \e[41m'"${PWD}"$'\e[0m [s/n]? ' dir
	echo ""
		case $dir in
			s|S) break ;;
			n|N) exit ;;
			*)  echo "Opcion no valida." ;;
		esac
	done
}

# Imprime original y modificado sin modificar los archivos
function _test_rename() {
	local file
	local newtitle
	for file in ./*; do
	    if [[ "$file" =~ .*\.avi$ || "$file" =~ .*\.mkv$ ]]; then
	    	newtitle="$(echo "${file##*/}" | sed -f "$STRINGS")"

	    	# Si coinciden los nombres continua al siguiente
		    if [[ "${file##*/}" == "$newtitle" ]]; then
			    continue
		    else
		    	# La opción 'Test' es el valor por defecto
		    	case "$TEST_RENAME" in
			    	T )
						echo -e "   ${file##*/}
			    		\r ${G}+ $(echo "${file##*/}" | sed -f "$STRINGS")${N}"
			    		sleep 0.1 ;;
			    	R )
						mv "${file}" "$newtitle" ;;
				esac
		    fi
        fi
	done
}

## MAIN ##

_correct_dir
_test_rename

while :; do
	read -r -p "[?] Continuar y renombrar los archivos [s/n]: " sino

	case $sino in
		[Ss])
			TEST_RENAME='R'
			_test_rename
			break ;;
		[Nn])
			echo "[X] Renombrado de archivos cancelado."
			exit ;;
		*)
			echo -e "[${R}!${N}] Opcion no valida inserte [${G}S${N}]i o [${G}N${N}]o :" ;;
	esac
done

echo -e "[${G}+${N}] Renombrado de archivos finalizado."
